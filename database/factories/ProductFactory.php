<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entities\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => $faker->randomFloat(2, 1, 15),
    ];
});
