<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\MarketService;

class MarketApiController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function showList() {
        $products = $this->marketService->getProductList();

        return $products->map(function ($product) {
            return new ProductResource($product);
        });
    }

    public function store(Request $request) {
        $product = $this->marketService->storeProduct($request);

        return new JsonResponse($product, 201);
    }

    public function showProduct(int $id) {
        return new ProductResource($this->marketService->getProductById($id));
    }

    public function delete(Request $request) {
        try {
            $product = $this->marketService->getProductById($request->id);

            $this->authorize('delete', $product);
            $this->marketService->deleteProduct($request);
        } catch (AuthorizationException $e) {
            return new JsonResponse('Only user that create product can delete it!', 403);
        }

        return new JsonResponse(null, 204);
    }
}
