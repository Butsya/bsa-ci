<?php

namespace Tests\Feature;

use App\Entities\Product;
use App\Services\MarketService;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiMarketServiceTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private $marketService;
    private $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->marketService = $this->app->make(MarketService::class);
    }

    public function test_show_list()
    {
        $products = factory(Product::class, 5)->create(['user_id' => $this->user->id])->map(function ($product) {
            return $product->only(['id', 'name', 'price', 'user_id']);
        });

        $this->get('/api/items')
            ->assertStatus(200)
            ->assertJson($products->toArray());
    }

    public function test_store()
    {
        $productData = [
            'name' => $this->faker->name,
            'price' => $this->faker->randomFloat(2, 1, 100),
            'user_id' => $this->user->id,
        ];
        $token = $this->user->api_token;
        $headers = ['Authorization' => "Bearer $token"];

        $this->actingAs($this->user)
            ->post('/api/items', $productData, $headers)
            ->assertStatus(201)
            ->assertJson($productData);
    }

    public function test_unauthenticated_user_cant_create_product()
    {
        $productData = [
            'name' => $this->faker->name,
            'price' => $this->faker->randomFloat(2, 1, 100),
            'user_id' => $this->user->id,
        ];

        $this->actingAs($this->user)
            ->post('/api/items', $productData)
            ->assertStatus(302);
    }

    public function test_show_product()
    {
        $product = factory(Product::class)->create(['user_id' => $this->user->id]);
        $expectedData = [
            'data' => $product->only(['id', 'name', 'price', 'user_id']),
        ];

        $this->get('/api/items/' . $product->id)
            ->assertStatus(200)
            ->assertJson($expectedData);
    }

    public function test_delete()
    {
        $product = factory(Product::class)->create(['user_id' => $this->user->id]);
        $token = $this->user->api_token;
        $headers = ['Authorization' => "Bearer $token"];

        $this->actingAs($this->user)
            ->delete('/api/items/' . $product->id, [], $headers)
            ->assertStatus(204);
    }

    public function test_another_user_cant_delete_product()
    {
        $anotherUser = factory(User::class)->create();
        $product = factory(Product::class)->create(['user_id' => $this->user->id]);
        $token = $anotherUser->api_token;
        $headers = ['Authorization' => "Bearer $token"];

        $this->actingAs($anotherUser)
            ->delete('/api/items/' . $product->id, [], $headers)
            ->assertStatus(403);
    }

    public function test_admin_can_delete_any_product()
    {
        $admin = factory(User::class)->create(['is_admin' => true]);
        $product = factory(Product::class)->create(['user_id' => $this->user->id]);
        $token = $admin->api_token;
        $headers = ['Authorization' => "Bearer $token"];

        $this->actingAs($admin)
            ->delete('/api/items/' . $product->id, [], $headers)
            ->assertStatus(204);
    }
}
