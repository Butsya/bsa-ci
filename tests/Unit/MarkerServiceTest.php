<?php

namespace Tests\Unit;

use App\Entities\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Services\MarketService;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

class MarkerServiceTest extends TestCase
{
    private $marketService;
    private $productRepositoryMock;

    protected function setUp(): void
    {
        parent::setUp();
        $this->productRepositoryMock = $this->createMock(ProductRepositoryInterface::class);
        $this->marketService = new MarketService($this->productRepositoryMock);
    }

    public function test_get_product_list()
    {
        $this->productRepositoryMock->method('findAll')
            ->willReturn($this->getProductCollection());

        $this->assertProductsData($this->marketService->getProductList());
    }

    private function assertProductsData(Collection $products): void
    {
        foreach ($products as $product) {
            $this->assertInstanceOf(Product::class, $product);
            $this->assertNotEmpty($product->id);
            $this->assertNotEmpty($product->name);
            $this->assertNotEmpty($product->price);
            $this->assertNotEmpty($product->user_id);
        }
    }

    public function test_get_product_by_id()
    {
        $expectedProduct = new Product([
            'id' => 1,
            'name' => 'product1',
            'price' => 400.21,
            'user_id' => 1,
        ]);
        $this->productRepositoryMock->method('findById')
            ->with(1)
            ->willReturn($expectedProduct);

        $actualProduct = $this->marketService->getProductById(1);

        $this->assertInstanceOf(Product::class, $actualProduct);
        $this->assertEquals($expectedProduct->price, $actualProduct->price);
        $this->assertEquals($expectedProduct->name, $actualProduct->name);
        $this->assertEquals($expectedProduct->user_id, $actualProduct->user_id);
    }

    public function test_get_products_by_user_id()
    {
        $userId = 1;
        $this->productRepositoryMock->method('findByUserId')
            ->with(1)
            ->willReturn($this->getUserProductCollection());

        $userProducts = $this->marketService->getProductsByUserId($userId);

        $this->assertCount(2, $userProducts);
        $this->assertProductsDataWithParticularUser($userProducts, $userId);
    }

    private function assertProductsDataWithParticularUser(Collection $products, int $userId): void
    {
        foreach ($products as $product) {
            $this->assertInstanceOf(Product::class, $product);
            $this->assertNotEmpty($product->id);
            $this->assertNotEmpty($product->name);
            $this->assertNotEmpty($product->price);
            $this->assertEquals($userId, $product->user_id);
        }
    }

    public function test_store_product()
    {
        $productData = [
            'name' => 'product1',
            'price' => 400.21,
            'user_id' => 1,
        ];
        $expectedProduct = new Product([
            'id' => 1,
            'name' => 'product1',
            'price' => 400.21,
            'user_id' => 1,
        ]);

        $this->productRepositoryMock->method('store')
            ->with(new Product($productData))
            ->willReturn($expectedProduct);

        $this->assertInstanceOf(Product::class, $expectedProduct);
        $this->assertEquals(1, $expectedProduct->id);
        $this->assertEquals(400.21, $expectedProduct->price);
        $this->assertEquals('product1', $expectedProduct->name);
        $this->assertEquals(1, $expectedProduct->user_id);
    }

    private function getProductCollection(): Collection
    {
        $products = [
            new Product(
                [
                    'id' => 2,
                    'name' => 'product2',
                    'price' => 200.22,
                    'user_id' => 2,
                ]
            ),
            new Product(
                [
                    'id' => 1,
                    'name' => 'product1',
                    'price' => 100.11,
                    'user_id' => 1,
                ]
            ),
            new Product(
                [
                    'id' => 3,
                    'name' => 'product1',
                    'price' => 300.33,
                    'user_id' => 1,
                ]
            ),
            new Product(
                [
                    'id' => 4,
                    'name' => 'product1',
                    'price' => 400.44,
                    'user_id' => 4,
                ]
            )
        ];
        $collection = new Collection();

        foreach ($products as $product) {
            $collection->add($product);
        }

        return $collection;
    }

    private function getUserProductCollection(): Collection
    {
        $products = [
            new Product(
                [
                    'id' => 1,
                    'name' => 'product1',
                    'price' => 100.11,
                    'user_id' => 1,
                ]
            ),
            new Product(
                [
                    'id' => 3,
                    'name' => 'product1',
                    'price' => 300.33,
                    'user_id' => 1,
                ]
            ),
        ];
        $collection = new Collection();

        foreach ($products as $product) {
            $collection->add($product);
        }

        return $collection;
    }
}
